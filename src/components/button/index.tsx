import React, { FC } from "react";

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  color: string;
}

const Button: FC<Props> = ({
  onClick,
  children,
  color,
  className = "",
  type = "button",
  disabled,
}) => (
  <button
    className={`flex-shrink-0 bg-${color}-500 hover:bg-${color}-700 border-${color}-500 hover:border-${color}-700 text-sm border-4 text-white py-1 px-2 rounded ${className} ${
      disabled && "opacity-50 cursor-not-allowed"
    }`}
    type={type}
    onClick={onClick}
    disabled={disabled}
  >
    {children}
  </button>
);

export default Button;
