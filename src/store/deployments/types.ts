export interface Version {
  name: string;
  _id: string;
  versions: Deployment[];
}

export interface VersionRes {
  edges: Version[];
  pageInfo: number;
}

export interface Deployment {
  url: string;
  templateName: string;
  version: string;
  deployedAt: string;
  _id: string;
}

export interface VersionInput {
  name: string;
  url: string;
  templateName: string;
  version: string;
}

export interface DeploymentInput {
  url: string;
  templateName: string;
  version: string;
  id: string;
}

export interface Pagination {
  page: number;
  perPage: number;
}

export interface VersionsState {
  versions: Version[];
  loadVersions: boolean;
  loadDeleteVersion: boolean;
  loadDeleteDeployments: boolean;
  loadAdd: boolean;
  total: number;
  pagination: Pagination;
}

// Describing the different ACTION NAMES available
export enum ActionsTypes {
  CHANGE_PAGE = "CHANGE_PAGE",
  SUCCESS_LOAD_VERSIONS = "SUCCESS_LOAD_VERSIONS",
  START_LOAD_VERSIONS = "START_LOAD_VERSIONS",
  FAILED_LOAD_VERSIONS = "FAILED_LOAD_VERSIONS",

  SUCCESS_CREATE_VERSIONS = "SUCCESS_CREATE_VERSIONS",
  START_CREATE_VERSIONS = "START_CREATE_VERSIONS",
  FAILED_CREATE_VERSIONS = "FAILED_CREATE_VERSIONS",

  SUCCESS_DELETE_VERSIONS = "SUCCESS_DELETE_VERSIONS",
  START_DELETE_VERSIONS = "START_DELETE_VERSIONS",
  FAILED_DELETE_VERSIONS = "FAILED_DELETE_VERSIONS",

  SUCCESS_DELETE_DEPLOYMENT = "SUCCESS_DELETE_DEPLOYMENT",
  START_DELETE_DEPLOYMENT = "START_DELETE_DEPLOYMENT",
  FAILED_DELETE_DEPLOYMENT = "FAILED_DELETE_DEPLOYMENT",

  SUCCESS_CREATE_DEPLOYMENT = "SUCCESS_CREATE_DEPLOYMENT",
  START_CREATE_DEPLOYMENT = "START_CREATE_DEPLOYMENT",
  FAILED_CREATE_DEPLOYMENT = "FAILED_CREATE_DEPLOYMENT",
}

interface StartLoadAction {
  type: ActionsTypes.START_LOAD_VERSIONS;
}

interface FailedLoadAction {
  type: ActionsTypes.FAILED_LOAD_VERSIONS;
}

interface SuccessLoadAction {
  type: ActionsTypes.SUCCESS_LOAD_VERSIONS;
  payload: VersionRes;
}

interface StartCreateAction {
  type: ActionsTypes.START_CREATE_VERSIONS;
}

interface FailedCreateAction {
  type: ActionsTypes.FAILED_CREATE_VERSIONS;
}

interface SuccessCreateAction {
  type: ActionsTypes.SUCCESS_CREATE_VERSIONS;
  payload: Version;
}

interface StartDeleteVersionsAction {
  type: ActionsTypes.START_DELETE_VERSIONS;
}

interface FailedDeleteVersionsAction {
  type: ActionsTypes.FAILED_DELETE_VERSIONS;
  payload: {
    msg: string;
  };
}

interface SuccessDeleteVersionsAction {
  type: ActionsTypes.SUCCESS_DELETE_VERSIONS;
  payload: Version[];
}

interface StartDeleteDeploymentAction {
  type: ActionsTypes.START_DELETE_DEPLOYMENT;
}

interface FailedDeleteDeploymentAction {
  type: ActionsTypes.FAILED_DELETE_DEPLOYMENT;
  payload: {
    msg: string;
  };
}

interface SuccessDeleteDeploymentAction {
  type: ActionsTypes.SUCCESS_DELETE_DEPLOYMENT;
}

interface StartCreateDeploymentAction {
  type: ActionsTypes.START_CREATE_DEPLOYMENT;
}

interface FailedCreateDeploymentAction {
  type: ActionsTypes.FAILED_CREATE_DEPLOYMENT;
  payload: {
    msg: string;
  };
}

interface SuccessCreateDeploymentAction {
  type: ActionsTypes.SUCCESS_CREATE_DEPLOYMENT;
}

interface ChangePage {
  type: ActionsTypes.CHANGE_PAGE;
  payload: number;
}

export type VersionsActionTypes =
  | SuccessLoadAction
  | StartLoadAction
  | FailedLoadAction
  | SuccessCreateAction
  | StartCreateAction
  | FailedCreateAction
  | StartDeleteVersionsAction
  | FailedDeleteVersionsAction
  | SuccessDeleteVersionsAction
  | StartDeleteDeploymentAction
  | FailedDeleteDeploymentAction
  | SuccessDeleteDeploymentAction
  | StartCreateDeploymentAction
  | FailedCreateDeploymentAction
  | SuccessCreateDeploymentAction
  | ChangePage;
