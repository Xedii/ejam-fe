import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

const Dashboard = lazy(() => import("pages/Dashboard"));

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <Route path="/" component={Dashboard} />
      </Router>
    </Suspense>
  );
}

export default App;
