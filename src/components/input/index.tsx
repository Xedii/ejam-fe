import React from "react";
import { FieldRenderProps } from "react-final-form";

interface IProps
  extends React.InputHTMLAttributes<HTMLInputElement>,
    FieldRenderProps<string, any> {
  label: string;
  field: FieldRenderProps<string, any>;
}
const Input: React.FC<IProps> = ({ input, meta, label, ...rest }) => {
  return (
    <>
      <div className="w-full px-3">
        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
          {label}
        </label>
        <input
          className={`appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 ${
            meta.touched && meta.error && "border-red-500"
          }`}
          id={input.name}
          {...input}
          {...rest}
        />
        {meta.touched && meta.error && (
          <p className="text-red-600 text-xs italic">{meta.error}</p>
        )}
      </div>
    </>
  );
};

export default Input;
