import { VersionsState, ActionsTypes, VersionsActionTypes } from "./types";

const initialState: VersionsState = {
  versions: [],
  loadVersions: false,
  loadDeleteVersion: false,
  loadDeleteDeployments: false,
  loadAdd: false,
  total: 0,
  pagination: {
    page: 1,
    perPage: 30,
  },
};

export function versionReducer(
  state = initialState,
  action: VersionsActionTypes
): VersionsState {
  switch (action.type) {
    case ActionsTypes.SUCCESS_LOAD_VERSIONS:
      return {
        ...state,
        versions: action.payload.edges,
        total: action.payload.pageInfo,
      };
    case ActionsTypes.CHANGE_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          page: action.payload,
        },
      };
    default:
      return state;
  }
}
