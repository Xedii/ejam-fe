import React, { useMemo } from "react";
import { Form, Field } from "react-final-form";
import useVersionsActions from "store/deployments/actions";
import { VersionInput } from "store/deployments/types";

import Input from "components/input";
import Button from "components/button";

import { validation } from "./utils";

const VersionForm = () => {
  const { createVersion } = useVersionsActions();
  const initialValues: VersionInput = useMemo(
    () => ({
      name: "",
      version: "",
      templateName: "",
      url: "",
    }),
    []
  );

  return (
    <Form
      onSubmit={createVersion}
      initialValues={initialValues}
      validate={validation}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form
          className="w-full max-w-lg"
          onSubmit={async (event) => {
            const error = await handleSubmit(event);
            if (error) {
              return error;
            }
            form.reset(initialValues);
            Object.keys(initialValues).map((field: any) =>
              form.resetFieldState(field)
            );
          }}
        >
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <Field
                name="name"
                component={Input}
                label="Name"
                placeholder="Name"
              />
            </div>
            <div className="w-full md:w-1/2 px-3">
              <Field
                name="templateName"
                component={Input}
                label="Template Name"
                placeholder="Alpha"
              />
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <Field
                name="version"
                component={Input}
                label="Version"
                placeholder="1.0.0"
              />
            </div>
            <div className="w-full md:w-1/2 px-3">
              <Field
                name="url"
                component={Input}
                label="url"
                placeholder="https://ejam.com"
              />
            </div>
          </div>
          <div className="w-full px-3 mb-6 md:mb-0 w-1/3 p-2">
            <Button
              type="submit"
              color="blue"
              disabled={submitting || pristine}
            >
              Add new
            </Button>
          </div>
        </form>
      )}
    />
  );
};

export default VersionForm;
