import React, { FC, useMemo } from "react";
import { Form, Field } from "react-final-form";
import { DeploymentInput } from "store/deployments/types";
import Input from "components/input";
import Button from "components/button";

import { validation } from "./utils";

interface Props {
  onSubmit(version: DeploymentInput): void;
  close(): void;
  versionId: string;
}

const DeploymentForm: FC<Props> = ({ onSubmit, close, versionId }) => {
  const initialValues: DeploymentInput = useMemo(
    () => ({
      version: "",
      templateName: "",
      url: "",
      id: versionId,
    }),
    []
  );

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      validate={validation}
      render={({ handleSubmit, submitting, pristine }) => (
        <form
          className="w-full max-w-lg"
          onSubmit={async (event) => {
            await handleSubmit(event);
            close();
          }}
        >
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
              <Field
                name="templateName"
                component={Input}
                label="Template Name"
                placeholder="Alpha"
              />
            </div>
            <div className="w-full md:w-1/2 px-3">
              <Field
                name="version"
                component={Input}
                label="Version"
                placeholder="1.0.0"
              />
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full md:w-1/2 px-3">
              <Field
                name="url"
                component={Input}
                label="url"
                placeholder="https://ejam.com"
              />
            </div>
          </div>
          <div className="w-full px-3 mb-6 md:mb-0 w-1/3 p-2">
            <Button
              disabled={submitting || pristine}
              type="submit"
              color="blue"
            >
              Save
            </Button>
          </div>
        </form>
      )}
    />
  );
};

export default DeploymentForm;
