import { TypedUseSelectorHook, useSelector, shallowEqual } from "react-redux";
import { AppState } from "store";

const useShallowEqualSelector: TypedUseSelectorHook<AppState> = (selector) =>
  useSelector(selector, shallowEqual);

export default useShallowEqualSelector;
