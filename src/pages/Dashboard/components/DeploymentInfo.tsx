import React, { FC, useCallback, memo } from "react";
import { useModal } from "react-modal-hook";
import Dialog from "./Dialog";

import Button from "components/button";

import { Deployment } from "store/deployments/types";

interface Props extends Deployment {
  removeDeployment(id: string): void;
}

const DeploymentInfo: FC<Props> = ({
  templateName,
  deployedAt,
  version,
  url,
  _id,
  removeDeployment,
}) => {
  const deleteDeployment = useCallback(() => {
    removeDeployment(_id);
    hideConfirmModal();
  }, [_id]);

  const [showConfirmModal, hideConfirmModal] = useModal(
    ({ in: open }) => (
      <Dialog
        actionText="Delete"
        onCancel={hideConfirmModal}
        onConfirm={deleteDeployment}
        title="Delete deployment?"
        open={open}
      >
        Do you really want to delete {templateName}?
      </Dialog>
    ),
    []
  );

  return (
    <div className="border-t mt-2">
      <div className="flex flex-wrap -mx-3 mt-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            Template name
          </label>
          <p className="text-grey-500 text-xs">{templateName}</p>
        </div>
        <div className="w-full md:w-1/2 px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            deployed At
          </label>
          <p className="text-grey-500 text-xs">
            {new Date(deployedAt).toDateString()}
          </p>
        </div>
      </div>
      <div className="flex flex-wrap -mx-3 mt-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            version
          </label>
          <p className="text-gray-600 text-xs italic">{version}</p>
        </div>
        <div className="w-full md:w-1/2 px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            url
          </label>
          <a href={url}>
            <p className="text-blue-500 text-xs">{url}</p>
          </a>
        </div>
      </div>
      <div className="flex flex-wrap -mx-3 mt-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <Button onClick={showConfirmModal} color="red">
            DELETE
          </Button>
        </div>
      </div>
    </div>
  );
};

export default memo(DeploymentInfo);
