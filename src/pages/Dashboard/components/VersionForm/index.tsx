import React from "react";
import Form from "./VersionForm";

const VersionForm = () => {
  return (
    <div className="flex bg-white shadow-lg rounded-lg my-3 mx-4 md:mx-auto max-w-md md:max-w-2xl ">
      <div className="flex items-start px-6 py-6 w-full">
        <div className="ease-in duration-200 w-full">
          <div className="flex items-center py-2">
            <Form />
          </div>
        </div>
      </div>
    </div>
  );
};

export default VersionForm;
