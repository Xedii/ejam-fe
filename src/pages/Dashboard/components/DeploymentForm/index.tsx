import React, { FC } from "react";
import Button from "components/button";
import { DeploymentInput } from "store/deployments/types";
import Form from "./From";

interface Props {
  onConfirm(version: DeploymentInput): void;
  onCancel(): void;
  open: boolean;
  versionId: string;
}

const index: FC<Props> = ({ onCancel, onConfirm, open, versionId }) => {
  if (!open) return null;
  return (
    <div className="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center">
      <div className="fixed inset-0 transition-opacity">
        <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
      </div>
      <div
        className="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full"
        role="dialog"
        aria-modal="true"
        aria-labelledby="modal-headline"
      >
        <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
          <div className="sm:flex sm:items-start">
            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
              <h3 className="text-lg leading-6 font-medium text-gray-900 text-center mb-4">
                Add new version
              </h3>
              <div className="mt-2">
                <Form
                  versionId={versionId}
                  close={onCancel}
                  onSubmit={onConfirm}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
          <span className="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
            <Button onClick={onCancel} color="gray">
              Close
            </Button>
          </span>
        </div>
      </div>
    </div>
  );
};

export default index;
