import React, { FC, memo } from "react";
import useVersionsActions from "store/deployments/actions";

import useShallowEqualSelector from "hooks/useShallowEqualSelector";
import VersionCard from "./VersionCard";
import Pagination from "components/pagination";

interface Props {}

const VersionList: FC<Props> = () => {
  const { changePage } = useVersionsActions();
  const versions = useShallowEqualSelector((state) => state.version.versions);
  const pagination = useShallowEqualSelector((state) => ({
    ...state.version.pagination,
    totalPages: Math.ceil(
      state.version.total / state.version.pagination.perPage
    ),
  }));

  return (
    <>
      {versions.map((version) => (
        <VersionCard
          key={version._id}
          name={version.name}
          deployments={version.versions}
          id={version._id}
        />
      ))}
      <Pagination
        onChange={changePage}
        numberOfPages={pagination.totalPages}
        initialPage={1}
      />
    </>
  );
};

export default memo(VersionList);
