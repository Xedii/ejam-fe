import React, { FC } from "react";
import useDashboard from "./useDashboard";
import VersionList from "./components/VersionList";

import From from "./components/VersionForm";

export const Dashboard: FC = () => {
  const { isLoading } = useDashboard();
  return (
    <>
      {isLoading && <p>Loading...</p>}
      <From />
      <VersionList />
    </>
  );
};

export default Dashboard;
