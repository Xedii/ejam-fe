import semverValid from "semver/functions/valid";
import isValidUrl from "is-url";
import { DeploymentInput } from "store/deployments/types";

export const validation = (values: DeploymentInput) => {
  const errors = {} as DeploymentInput;
  if (!values.templateName) {
    errors.templateName = "Required";
  }
  if (!values.version) {
    errors.version = "Required";
  }
  if (!semverValid(values.version)) {
    errors.version = "Version is not valid";
  }
  if (!values.url) {
    errors.url = "Required";
  }

  if (!isValidUrl(values.url)) {
    errors.url = "URL is not valid";
  }

  return errors;
};
