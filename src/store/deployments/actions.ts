import { useDispatch } from "react-redux";
import { useAlert } from "react-alert";
import useShallowEqualSelector from "hooks/useShallowEqualSelector";

import {
  getVersions,
  addVersion,
  addDeployment,
  deleteVersion,
  deleteDeployment,
} from "utils/api";

import {
  Pagination,
  VersionInput,
  DeploymentInput,
  ActionsTypes,
} from "./types";

const useDeploymentsActions = () => {
  const dispatch = useDispatch();
  const alert = useAlert();
  const pagination = useShallowEqualSelector(
    (state) => state.version.pagination
  );

  const loadVersions = async (pagination: Pagination) => {
    dispatch({
      type: ActionsTypes.START_LOAD_VERSIONS,
    });
    try {
      const payload = await getVersions(pagination.page);
      dispatch({
        type: ActionsTypes.SUCCESS_LOAD_VERSIONS,
        payload,
      });
    } catch (error) {
      dispatch({
        type: ActionsTypes.FAILED_LOAD_VERSIONS,
      });
      alert.error("List download failed");
    }
  };

  const createVersion = async (version: VersionInput) => {
    dispatch({
      type: ActionsTypes.START_CREATE_VERSIONS,
    });
    try {
      console.log(version);
      await addVersion(version);
      dispatch({
        type: ActionsTypes.SUCCESS_CREATE_VERSIONS,
      });
      alert.success("Created version!");
      loadVersions(pagination);
    } catch (error) {
      dispatch({
        type: ActionsTypes.FAILED_CREATE_VERSIONS,
      });
      alert.error("Something going wrong");
    }
  };

  const removeVersions = async (id: string) => {
    dispatch({
      type: ActionsTypes.START_DELETE_VERSIONS,
    });
    try {
      await deleteVersion(id);
      dispatch({
        type: ActionsTypes.SUCCESS_DELETE_VERSIONS,
      });
      alert.success("Version deleted");
      loadVersions(pagination);
    } catch (error) {
      dispatch({
        type: ActionsTypes.FAILED_DELETE_VERSIONS,
      });

      alert.error("Can not delete version");
    }
  };

  const removeDeployment = async (id: string) => {
    dispatch({
      type: ActionsTypes.START_DELETE_DEPLOYMENT,
    });
    try {
      await deleteDeployment(id);
      dispatch({
        type: ActionsTypes.SUCCESS_DELETE_DEPLOYMENT,
      });
      alert.success("Deployment deleted");
      loadVersions(pagination);
    } catch (error) {
      dispatch({
        type: ActionsTypes.FAILED_DELETE_DEPLOYMENT,
      });
      alert.error("Can not delete deployment");
    }
  };

  const createDeployment = async (version: DeploymentInput) => {
    dispatch({
      type: ActionsTypes.START_CREATE_VERSIONS,
    });
    try {
      await addDeployment(version);
      dispatch({
        type: ActionsTypes.SUCCESS_CREATE_VERSIONS,
      });
      alert.success("Created deployment!");
      loadVersions(pagination);
    } catch (error) {
      dispatch({
        type: ActionsTypes.FAILED_CREATE_VERSIONS,
      });
      alert.error("Can not delete deployment");
    }
  };

  const changePage = (page: number) => {
    dispatch({
      type: ActionsTypes.CHANGE_PAGE,
      payload: page,
    });
  };

  return {
    loadVersions,
    createVersion,
    removeVersions,

    removeDeployment,
    createDeployment,

    changePage,
  };
};

export default useDeploymentsActions;
