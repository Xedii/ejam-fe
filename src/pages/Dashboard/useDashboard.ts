import { useEffect } from "react";
import useVersionsActions from "store/deployments/actions";
import useShallowEqualSelector from "hooks/useShallowEqualSelector";

const useDashboard = () => {
  const { loadVersions } = useVersionsActions();
  const pagination = useShallowEqualSelector(
    (state) => state.version.pagination
  );
  const isLoading = useShallowEqualSelector(
    (state) => state.version.loadVersions
  );

  useEffect(() => {
    loadVersions(pagination);
  }, [pagination]);

  return {
    pagination,
    isLoading,
  };
};

export default useDashboard;
