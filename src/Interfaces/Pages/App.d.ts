// #region Global Imports
import { Store } from 'redux';

// #endregion Global Imports
declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production' | 'test';
    PUBLIC_URL: string;
    REACT_APP_API_URI: string;
  }
}

interface Window {
  Stripe: any;
}

interface AppStore extends Store {}

export interface AppWithStore extends AppInitialProps {
  store: AppStore;
}
