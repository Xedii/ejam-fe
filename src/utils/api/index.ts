import axios from "./axios";
import {
  Version,
  VersionInput,
  DeploymentInput,
  VersionRes,
} from "store/deployments/types";

export const getVersions = async (
  page: number,
  perPage: number = 30
): Promise<VersionRes[]> => {
  const { data } = await axios.get(`/version?page=${page}&perPage=${perPage}`);
  return data;
};

export const addVersion = async (version: VersionInput): Promise<Version> => {
  const { data } = await axios.post(`/version`, version);
  return data;
};

export const deleteVersion = async (id: string): Promise<successMsg> => {
  const { data } = await axios.delete(`/version/${id}`);
  return data;
};

export const addDeployment = async ({
  id,
  ...version
}: DeploymentInput): Promise<Version> => {
  const { data } = await axios.post(`/deployment/${id}`, version);
  return data;
};

export const deleteDeployment = async (id: string): Promise<successMsg> => {
  const { data } = await axios.delete(`/deployment/${id}`);
  return data;
};
