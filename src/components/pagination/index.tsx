import React, { FC } from "react";
import { usePagination, ConfigArg } from "react-pagination-hook";

interface Props extends ConfigArg {
  onChange(page: number): void;
}

const Pagination: FC<Props> = ({
  initialPage,
  numberOfPages,
  maxButtons = 5,
  onChange,
}) => {
  const { activePage, visiblePieces, goToPage } = usePagination({
    initialPage,
    numberOfPages,
    maxButtons,
  });

  return (
    <div className="flex bg-white my-3 mx-4 md:mx-auto max-w-md md:max-w-2xl ">
      <div className="flex items-start px-6 py-6 w-full justify-center">
        <ul className="flex list-reset border border-grey-light rounded w-auto font-sans">
          {visiblePieces.map((visiblePiece, index) => {
            const key = `${visiblePiece.type}-${index}`;
            if (visiblePiece.type === "ellipsis") {
              return (
                <li key={key}>
                  <p className="block hover:text-blue-800 hover:bg-blue text-blue border-r border-grey-light px-3 py-2 cursor-pointer">
                    ...
                  </p>
                </li>
              );
            }

            const { pageNumber } = visiblePiece;
            const onClick = () => {
              goToPage(pageNumber);
              onChange(pageNumber);
            };

            if (visiblePiece.type === "page-number") {
              const isActive = pageNumber === activePage;
              const className = isActive ? "text-blue-600" : "";
              return (
                <li key={key}>
                  <p
                    onClick={onClick}
                    className={`block hover:text-blue-800 hover:bg-blue text-blue border-r border-grey-light px-3 py-2 cursor-pointer ${className}`}
                  >
                    {pageNumber}
                  </p>
                </li>
              );
            }

            return (
              <li key={key}>
                <p
                  onClick={onClick}
                  className="block hover:text-blue-800 hover:bg-blue text-blue px-3 py-2 cursor-pointer"
                >
                  {visiblePiece.type === "next" ? "Next" : "Previous"}
                </p>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Pagination;
