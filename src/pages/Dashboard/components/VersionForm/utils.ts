import semverValid from "semver/functions/valid";
import { VersionInput } from "store/deployments/types";
import isValidUrl from "is-url";

export const validation = (values: VersionInput) => {
  const errors = {} as VersionInput;
  if (!values.name) {
    errors.name = "Required";
  }
  if (!values.templateName) {
    errors.templateName = "Required";
  }
  if (!values.version) {
    errors.version = "Required";
  }
  if (!semverValid(values.version)) {
    errors.version = "Version is not valid";
  }

  if (!isValidUrl(values.url)) {
    errors.url = "URL is not valid";
  }

  if (!values.url) {
    errors.url = "Required";
  }

  return errors;
};
