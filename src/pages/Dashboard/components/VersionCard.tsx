import React, { FC, useCallback } from "react";
import { useModal } from "react-modal-hook";

import { Deployment } from "store/deployments/types";
import useDeploymentsActions from "store/deployments/actions";
import Button from "components/button";

import DeploymentInfo from "./DeploymentInfo";
import Dialog from "./Dialog";
import DeploymentDialog from "./DeploymentForm";

interface Props {
  id: string;
  name: string;
  deployments: Deployment[];
}

const VersionCard: FC<Props> = ({ name, id, deployments }) => {
  const {
    removeDeployment,
    removeVersions,
    createDeployment,
  } = useDeploymentsActions();

  const deleteDeployment = useCallback(() => {
    removeVersions(id);
    hideConfirmModal();
  }, [id]);

  const [showConfirmModal, hideConfirmModal] = useModal(
    ({ in: open }) => (
      <Dialog
        actionText="Delete"
        onCancel={hideConfirmModal}
        onConfirm={deleteDeployment}
        title="Delete version?"
        open={open}
      >
        Do you really want to delete {name}?
      </Dialog>
    ),
    []
  );

  const [showNewDeployModal, hideNewDeployModal] = useModal(
    ({ in: open }) => (
      <DeploymentDialog
        versionId={id}
        onCancel={hideNewDeployModal}
        onConfirm={createDeployment}
        open={open}
      />
    ),
    []
  );

  return (
    <div className="flex bg-white shadow-lg rounded-lg my-3 mx-4 md:mx-auto max-w-md md:max-w-2xl ">
      <div className="flex items-start px-6 py-6 w-full">
        <div className="w-full">
          <div className="flex items-center justify-between">
            <h2 className="text-lg font-semibold text-gray-900 -mt-1">
              {name}
            </h2>
            <div className="flex items-center justify-between">
              <Button onClick={showNewDeployModal} color="blue">
                New Add
              </Button>
              <Button onClick={showConfirmModal} className="ml-3" color="red">
                Delete
              </Button>
            </div>
          </div>
          <div className="mt-3">
            {deployments.map((deployment) => (
              <DeploymentInfo
                removeDeployment={removeDeployment}
                {...deployment}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default VersionCard;
